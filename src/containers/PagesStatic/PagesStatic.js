import React, {Component} from 'react';
import Pages from "../../components/Pages/Pages";
import {Row} from "reactstrap";

import axios from "../../axios-orders";


class PagesStatic extends Component {
  state = {
    title: "",
    description: "",
    image: "",
    response: null
  };

  componentDidMount() {
    axios.get(`${this.props.match.params.text}.json`).then(response => {
        const data = response.data;
        this.setState({data: data, title: data.title, description: data.description, image: data.image})
    })
  }


  render() {
    return (
      <div>
        <Row>
          {this.state.data ? <Pages image={this.state.image} title={this.state.title} description={this.state.description}/> : null}
        </Row>
      </div>
    );
  }
}

export default PagesStatic;