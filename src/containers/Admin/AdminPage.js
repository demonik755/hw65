import React, {Component} from 'react';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

import axios from "../../axios-orders";

class AdminPage extends Component {
    state = {
        title: "",
        description: "",
        response: null,
        value: ""
    };
    handleChange = event => {
        let name = event.target.name;
        let value = event.target.value;
        axios.get(`${value}.json`).then(response => {
            console.log(response);
            this.setState({title: response.data.title, description: response.data.description});
        });
        this.setState({[name]: value,value: event.target.value});
    };
    fireBasePatch = () => {
        axios.patch(`${this.state.value}.json`).then(response => {
            const data = response.data;
            console.log(response);
            this.setState({title: data.title, description: data.description, });
        });
    };
    render() {
        console.log(this.state.title);
        console.log(this.state.value);
        console.log(this.state.description);
        return (
                <Form>
                    <FormGroup>
                        <Label for="exampleSelect">Select page</Label>
                        <Input onChange={(event)=>this.handleChange(event)} value={this.state.value} type="select" name="select" id="exampleSelect">
                            <option value="About">About</option>
                            <option value="Contacts">Contacts</option>
                            <option value="Cinema">Cinema</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleText">description</Label>
                        <Input onChange={(event)=>this.handleChange(event)} value={this.state.description}   type="textarea" name="description" id="exampleText" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleTitle">title</Label>
                        <Input onChange={(event)=>this.handleChange(event)}  value={this.state.title}  type="input" name="title" id="exampleTitle"  />
                    </FormGroup>
                    <Button onSubmit={()=>this.fireBasePatch()} >Save</Button>
                </Form>

        );
    }
}

export default AdminPage;