import React, {useState} from 'react';
import {Collapse, Nav, Navbar, NavbarToggler, NavItem, NavLink} from "reactstrap";

const Layout = props => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/pages/about">About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/pages/contacts">Contacts</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/pages/cinema">Cinema</NavLink>
            </NavItem>
            <NavItem >
              <NavLink  href="/AdminPage">Admin</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Layout;