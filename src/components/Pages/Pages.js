import React from 'react';
import {Card, CardBody, CardImg, CardText, CardTitle} from "reactstrap";

import "./Pages.css";


const Pages = props => {
  return (

          <Card sm="6">
              <CardImg top src={props.image} alt="Card image cap" className="Image" />
              <CardBody>
                  <CardTitle>{props.title}</CardTitle>
                  <CardText>{props.description}</CardText>
              </CardBody>
          </Card>



  );
};

export default Pages;