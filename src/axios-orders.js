import axios from "axios";

const instance = axios.create({
    baseURL: "https://homework65-c6f5b.firebaseio.com/",
});

export default instance;