import React, {Fragment} from 'react';
import './App.css';
import Layout from "./components/Layout/Layout";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import PagesStatic from "./containers/PagesStatic/PagesStatic";
import {Container} from "reactstrap";
import AdminPage from "./containers/Admin/AdminPage";

function App() {
  return (
    <Fragment>
      <Layout/>
      <Container>
        <BrowserRouter>
          <Switch>
            <Route path="/pages/:text" component={PagesStatic}/>
            <Route path="/AdminPage" exact  component={AdminPage}/>
          </Switch>
        </BrowserRouter>
      </Container>
    </Fragment>

  );
}

export default App;
